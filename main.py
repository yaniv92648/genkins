import requests
import os


def main():
    urls = os.getenv("urls")
    for url in urls.split(','):
        print(requests.get(url).text)


if __name__ == '__main__':
    main()
