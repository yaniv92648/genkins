FROM python:3.6-jessie
#This line will make docker use the caches requirements
WORKDIR /opt
ADD / /opt
RUN pip install -r requirements.txt

ENTRYPOINT ["python", "-u" , "/opt/main.py"]